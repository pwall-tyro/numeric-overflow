package app;

import java.math.BigInteger;
import java.util.Arrays;

public class Main {

    private BigInteger threshold = BigInteger.valueOf(1000);

    public static void main(String[] args) {

        Main app = new Main();

        int amount = 999;
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = 2000;
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(int amount){
        return approval(BigInteger.valueOf(amount));
    }

    public boolean approval(BigInteger amount){
        return amount.compareTo(threshold) >= 0;
    }

}
